package edu.buffalo.cse116;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Random;

public class ClueGame {
	// this is where we will initilize the board
	// I have put all the things in ArrayList
	static int ROW = 13;
	static int COL = ROW;

	public int _diceNum = 0;

	public ArrayList<ArrayList<Tile>> _board;
	public ArrayList<Player> _playerList;
	public ArrayList<Weaponcards> _weaponcardsList;
	public ArrayList<Playercards> _playercardsList;
	public ArrayList<Roomcards> _roomcardsList;
	public ArrayList<Rooms> _roomsList;

	public ClueGame() {
		_diceNum = rollDice();

		_board = new ArrayList<ArrayList<Tile>>();
		for (int i = 0; i < ROW; i++) {
			ArrayList<Tile> colTileArr = new ArrayList<>();
			for (int j = 0; j < COL; j++) {
				colTileArr.add(j, new Tile(i, j));
			}
			_board.add(i, colTileArr);
		}
		setPlayer();
		setWeaponcards();
		setPlayercards();
		setRoomcards();
		setRooms();
	}

	public void setPlayer() {

		_playerList = new ArrayList<>();

		Integer[] p1Loc = { 3, 3 };
		Player p1 = new Player(1, Color.BLUE, p1Loc);

		Integer[] p2Loc = { 3, 4 };
		Player p2 = new Player(2, Color.BLACK, p2Loc);

		Integer[] p3Loc = { 3, 5 };
		Player p3 = new Player(3, Color.YELLOW, p3Loc);

		Integer[] p4Loc = { 3, 6 };
		Player p4 = new Player(4, Color.RED, p4Loc);

		Integer[] p5Loc = { 3, 7 };
		Player p5 = new Player(5, Color.GREEN, p5Loc);

		Integer[] p6Loc = { 3, 8 };
		Player p6 = new Player(6, Color.CYAN, p6Loc);

		_playerList.add(p1);
		_playerList.add(p2);
		_playerList.add(p3);
		_playerList.add(p4);
		_playerList.add(p5);
		_playerList.add(p6);

	}

	public void setWeaponcards() { // 6 cards

		_weaponcardsList = new ArrayList<>();

		Weaponcards w1 = new Weaponcards("Poison");
		Weaponcards w2 = new Weaponcards("Knife");
		Weaponcards w3 = new Weaponcards("Wrench");
		Weaponcards w4 = new Weaponcards("Revolver");
		Weaponcards w5 = new Weaponcards("Rope");
		Weaponcards w6 = new Weaponcards("Candle stick");
		_weaponcardsList.add(w1);
		_weaponcardsList.add(w2);
		_weaponcardsList.add(w3);
		_weaponcardsList.add(w4);
		_weaponcardsList.add(w5);
		_weaponcardsList.add(w6);
	}

	public void setPlayercards() { // 6 cards

		_playercardsList = new ArrayList<>();

		Playercards p1 = new Playercards("Ms. Vivienne Scarlet");
		Playercards p2 = new Playercards("Col. Michael Mustard");
		Playercards p3 = new Playercards("Mrs. Blanche White");
		Playercards p4 = new Playercards("Rev. Jonathan Green");
		Playercards p5 = new Playercards("Mrs. Elizabeth Peacock");
		Playercards p6 = new Playercards("Prof. Peter Plum");
		_playercardsList.add(p1);
		_playercardsList.add(p2);
		_playercardsList.add(p3);
		_playercardsList.add(p4);
		_playercardsList.add(p5);
		_playercardsList.add(p6);
	}

	public void setRoomcards() { // 9 cards

		_roomcardsList = new ArrayList<>();

		Roomcards r1 = new Roomcards("Ballroom");
		Roomcards r2 = new Roomcards("Billard Room");
		Roomcards r3 = new Roomcards("Conservatory");
		Roomcards r4 = new Roomcards("Dining Room");
		Roomcards r5 = new Roomcards("Hall");
		Roomcards r6 = new Roomcards("Kitchen");
		Roomcards r7 = new Roomcards("Library");
		Roomcards r8 = new Roomcards("Lounge");
		Roomcards r9 = new Roomcards("Study");
		_roomcardsList.add(r1);
		_roomcardsList.add(r2);
		_roomcardsList.add(r3);
		_roomcardsList.add(r4);
		_roomcardsList.add(r5);
		_roomcardsList.add(r6);
		_roomcardsList.add(r7);
		_roomcardsList.add(r8);
		_roomcardsList.add(r9);
	}

	public int rollDice() {
		Random r = new Random();
		Dice d1 = new Dice(r.nextInt(6) + 1);// i tested this in different class
												// // and i think this is right
		return d1._diceNum;
	}

	public void setRooms() {
		// room 1
		ArrayList<Integer[]> roomTiles = new ArrayList<>();
		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				Integer[] tilesInRoom = { i, j };
				roomTiles.add(tilesInRoom);
			}
		}
		Rooms R1 = new Rooms("BallRoom", roomTiles);

		// room 2
		roomTiles = (ArrayList<Integer[]>) roomTiles.clone();
		for (int i = 0; i < roomTiles.size(); i++) {
			roomTiles.get(i)[0] += 5;
		}
		Rooms R2 = new Rooms("Billiard Room", roomTiles);

		// room 3
		roomTiles = (ArrayList<Integer[]>) roomTiles.clone();
		for (int i = 0; i < roomTiles.size(); i++) {
			roomTiles.get(i)[0] += 5;
		}
		Rooms R3 = new Rooms("Conservatory", roomTiles);

		// room 4
		for (int i = 0; i < 3; i++) {
			for (int j = 5; j < 8; j++) {
				Integer[] tilesInRoom = { i, j };
				roomTiles.add(tilesInRoom);
			}
		}
		Rooms R4 = new Rooms("Dining Room", roomTiles);

		// room 5
		roomTiles = (ArrayList<Integer[]>) roomTiles.clone();
		for (int i = 0; i < roomTiles.size(); i++) {
			roomTiles.get(i)[0] += 5;
		}
		Rooms R5 = new Rooms("Hall", roomTiles);

		// room 6
		roomTiles = (ArrayList<Integer[]>) roomTiles.clone();
		for (int i = 0; i < roomTiles.size(); i++) {
			roomTiles.get(i)[0] += 5;
		}
		Rooms R6 = new Rooms("Kitchen", roomTiles);

		// room 7
		for (int i = 0; i < 3; i++) {
			for (int j = 10; j < 13; j++) {
				Integer[] tilesInRoom = { i, j };
				roomTiles.add(tilesInRoom);
			}
		}
		Rooms R7 = new Rooms("Library", roomTiles);

		// room 8
		roomTiles = (ArrayList<Integer[]>) roomTiles.clone();
		for (int i = 0; i < roomTiles.size(); i++) {
			roomTiles.get(i)[0] += 5;
		}
		Rooms R8 = new Rooms("Lounge", roomTiles);

		// room 9
		roomTiles = (ArrayList<Integer[]>) roomTiles.clone();
		for (int i = 0; i < roomTiles.size(); i++) {
			roomTiles.get(i)[0] += 5;
		}
		Rooms R9 = new Rooms("Study", roomTiles);

		_roomsList = new ArrayList<>();
		_roomsList.add(R1);
		_roomsList.add(R2);
		_roomsList.add(R3);
		_roomsList.add(R4);
		_roomsList.add(R5);
		_roomsList.add(R6);
		_roomsList.add(R7);
		_roomsList.add(R8);
		_roomsList.add(R9);

	}

	public boolean movePlayer(Integer[] pLoc, Integer r, Integer c, int diceNum) {
		Integer[] tempLoc = pLoc;

		// unplacable position
		if (r > 13 || c > 13 || r < 0 || c < 0) {
			return false;
		}

		while (diceNum != 0 && !(pLoc[0] == r && pLoc[1] == c)) {
			System.out.println("pLoc : " + pLoc[0] + " / " + pLoc[1]);
			System.out.println("dice : " + diceNum + "  /  r, c : " + r + ", " + c);
			System.out.println("outer");
			while (tempLoc[1] != c || tempLoc[0] != r) {
				System.out.println("inner");
				if (tempLoc[0] < r) { // row case
					tempLoc[0]++;
					movePlayer(tempLoc, r, c, diceNum - 1);
				} else if (tempLoc[0] > r) {
					tempLoc[0]--;
					movePlayer(tempLoc, r, c, diceNum - 1);
				}
				if (tempLoc[1] < c) { // column case
					tempLoc[1]++;
					movePlayer(tempLoc, r, c, diceNum - 1);
				} else if (tempLoc[1] > c) {
					tempLoc[1]--;
					movePlayer(tempLoc, r, c, diceNum - 1);
				}
			}
		}

		if (tempLoc[0] == r && tempLoc[1] == c) {
			System.out.println("here");
			System.out.println(r + " , " + c);
			pLoc[0] = r;
			pLoc[1] = c;
			return true;
		} else {
			return false;
		}
	}
}
