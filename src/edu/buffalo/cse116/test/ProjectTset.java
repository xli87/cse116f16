package edu.buffalo.cse116.test;

import edu.buffalo.cse116.*;

import static org.junit.Assert.*;

import java.util.Arrays;

import org.junit.Test;

public class ProjectTset {

	@Test
	public void testMoveVertical() {

		ClueGame game = new ClueGame();
		int dice = game._diceNum;

		Player testPlayer = game._playerList.get(0); // loc = {3,3}
		Integer[] playerLoc = testPlayer._playerLocation;

		int expected = playerLoc[0] + dice; // expected as row + dice
		game.movePlayer(playerLoc, playerLoc[0] + dice, playerLoc[1], dice);

		// expecting row = row + dice number ( ex. loc = [3,3] & dice = 5 , then
		// loc = [8,3]
		assertTrue("row was " + playerLoc[0] + " and dice was " + dice + ", so expected value : " + expected
				+ " , but actual was : " + playerLoc[0], playerLoc[0] == expected);

	}

	@Test
	public void testMoveHorizontal() {

		ClueGame game = new ClueGame();
		int dice = game._diceNum;

		Player testPlayer = game._playerList.get(0); // loc = {3,3}
		Integer[] playerLoc = testPlayer._playerLocation;

		int expected = playerLoc[1] + dice; // expected as col + dice
		game.movePlayer(playerLoc, playerLoc[0], playerLoc[1] + dice, dice);

		// expecting row = row + dice number ( ex. loc = [3,3] & dice = 5 , then
		// loc = [3,8]
		assertTrue("col was " + playerLoc[1] + " and dice was " + dice + ", so expected value : " + expected
				+ " , but actual was : " + playerLoc[1], playerLoc[1] == expected);
	}

	@Test
	public void testMoveVerticalAndHorizontal() {
		ClueGame game = new ClueGame();
		int dice = game._diceNum;

		Player testPlayer = game._playerList.get(0); // loc = {3,3}
		Integer[] playerLoc = testPlayer._playerLocation;

		int diceForRow = 1; // 1 for testing row
		int diceForCol = dice - 1; // dice - 1 for testing row

		int expectedRow = playerLoc[0] + diceForRow; // expected as row + dice
		int expectedCol = playerLoc[1] + diceForCol;

		Integer[] expected = { playerLoc[0] + diceForRow, playerLoc[1] + diceForCol };
		game.movePlayer(playerLoc, playerLoc[0] + diceForRow, playerLoc[1] + diceForCol, dice);

		// expecting loc = { row + 1, col + (dice - 1) } ( ex. loc = [3,3] &
		// dice = 5 , then
		// loc = [4,7]
		assertTrue(
				"row was " + playerLoc[0] + " col was " + playerLoc[1] + " and dice for row was " + diceForRow
						+ " and dice for col was " + diceForCol + " so the expected value is " + expected[0] + " / "
						+ expected[1] + " , but the actual was : " + playerLoc[0] + " / " + playerLoc[1],
				Arrays.equals(playerLoc, expected));

	}

	@Test
	public void testDiagonol(){
		ClueGame game = new ClueGame();
		
		Player testPlayer = game._playerList.get(0); // loc = {3,3}
		Integer[] playerLoc = testPlayer._playerLocation;
		
		int dice = 1;

		boolean actual = game.movePlayer(playerLoc, playerLoc[0]+1, playerLoc[1]+1, dice); // trying to move to {4,4} with dice 1
		
		boolean expected = false; 
		 
		
		assertFalse("expected : " +expected + " but actual was" + actual , actual = expected);
	}

}
